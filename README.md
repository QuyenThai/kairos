## Getting Started

#### Install dependencies:

```bash
npm
```

#### Set environment variables:

```bash
npm install
```

```bash
cp .env.example .env
```

`then`

update the .env

## Running Locally

```bash
npm run dev
```
