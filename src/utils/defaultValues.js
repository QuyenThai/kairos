const USER_ROLE = {
  ADMIN: 1,
  USER: 2,
};

module.exports = { USER_ROLE };
