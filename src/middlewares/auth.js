const passport = require("passport");
const { promisify } = require("es6-promisify");
const { USER_ROLE } = require("../utils/defaultValues");
const AccessToken = require("../models/acessToken.model");
const mongoose = require("mongoose");

const handleJWT = (req, res, next, roles) => async (err, user, info) => {
  const error = err || info;
  const logIn = promisify(req.logIn);
  let apiError = new Error("Unauthorized");
  try {
    if (error || !user) throw error;
    await logIn(user, { session: false });
  } catch (e) {
    return next(apiError);
  }

  const accessToken = await AccessToken.findOne({
    userId: mongoose.Types.ObjectId(user._id),
  }).sort({ expiresIn: -1 });

  if (!accessToken) {
    apiError = new Error("jwt expired");
    return next(apiError);
  }
  if (!roles.includes(user.role)) {
    apiError = new Error("Forbidden");
    return next(apiError);
  } else if (err || !user) {
    return next(apiError);
  }

  req.user = user;

  return next();
};

exports.authorize =
  (roles = Object.values(USER_ROLE)) =>
  (req, res, next) =>
    passport.authenticate(
      "jwt",
      { session: false },
      handleJWT(req, res, next, roles)
    )(req, res, next);
