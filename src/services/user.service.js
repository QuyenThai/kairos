const User = require("../models/user.model");

const getListFilByName = async (filter) => {
  const { name } = filter;
  return User.find({ name }).select([
    "_id",
    "name",
    "email",
    "phoneNumber",
    "role",
    "createdAt",
  ]);
};

module.exports = {
  getListFilByName,
};
