const mongoose = require("mongoose");

/**
 * Accesstoken Schema
 * @private
 */
const accessTokenSchema = new mongoose.Schema({
  accessToken: {
    type: String,
    required: true,
    index: true,
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
  expiresIn: { type: Date },
});

accessTokenSchema.statics = {
  async generate(user, token) {
    const userId = user._id;
    const { accessToken, expiresIn } = token;
    const savedToken = await this.create({ accessToken, userId, expiresIn });
    return savedToken;
  },
};

/**
 * @typedef accessTokenSchema
 */
module.exports = mongoose.model("accessToken", accessTokenSchema);
