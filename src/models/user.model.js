const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const moment = require("moment-timezone");
const jwt = require("jwt-simple");
const { env, jwtSecret, jwtExpirationInterval } = require("../config/vars");
const { USER_ROLE } = require("../utils/defaultValues");

const roles = Object.values(USER_ROLE);

/**
 * User Schema
 * @private
 */
const userSchema = new mongoose.Schema(
  {
    email: {
      type: String,
      match: /^\S+@\S+\.\S+$/,
      required: true,
      unique: true,
      trim: true,
      lowercase: true,
    },
    password: {
      type: String,
      required: true,
      minlength: 6,
      maxlength: 40,
    },
    name: {
      type: String,
      maxlength: 128,
      index: true,
      trim: true,
    },
    phoneNumber: {
      type: String,
      maxlength: 20,
      trim: true,
    },
    role: {
      type: Number,
      enum: roles,
      default: USER_ROLE.USER,
    },
  },
  {
    timestamps: true,
  }
);

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */
userSchema.pre("save", async function save(next) {
  try {
    if (!this.isModified("password")) return next();

    const rounds = env === "test" ? 1 : 10;

    const hash = await bcrypt.hash(this.password, rounds);
    this.password = hash;

    return next();
  } catch (error) {
    return next(error);
  }
});

/**
 * Methods
 */
userSchema.method({
  transform() {
    const transformed = {};
    const fields = ["id", "name", "email", "phoneNumber", "role", "createdAt"];

    fields.forEach((field) => {
      transformed[field] = this[field];
    });

    return transformed;
  },

  token() {
    const playload = {
      exp: moment().add(jwtExpirationInterval, "minutes").unix(),
      iat: moment().unix(),
      sub: this._id,
    };
    return jwt.encode(playload, jwtSecret);
  },

  async passwordMatches(password) {
    return bcrypt.compare(password, this.password);
  },
});

/**
 * Statics
 */
userSchema.statics = {
  /**
   * Find user by email and tries to generate a JWT token
   */
  async findAndGenerateToken(options) {
    const { email, password } = options;
    if (!email) throw new Error("An email is required to generate a token");

    const user = await this.findOne({ email }).exec();
    let err = "UNAUTHORIZED";
    if (password) {
      if (user && (await user.passwordMatches(password))) {
        return { user, accessToken: user.token() };
      }
      err = "Incorrect email or password";
    } else {
      err = "Incorrect email or refreshToken";
    }
    throw new Error(err);
  },

  checkDuplicateEmail(error) {
    if (error.name === "MongoError" && error.code === 11000) {
      return new Error('"email" already exists');
    }
    return error;
  },
};

/**
 * @typedef User
 */
module.exports = mongoose.model("User", userSchema);
