const mongoose = require("mongoose");
const User = require("../models/user.model");
const { getListFilByName } = require("../services/user.service");

/**
 * Create new user
 */
exports.create = async (req, res, next) => {
  try {
    const user = new User(req.body);
    const savedUser = await user.save();
    res.json(savedUser.transform());
  } catch (error) {
    return next(User.checkDuplicateEmail(error));
  }
};

/**
 * Get list user filter by name
 */
exports.list = async (req, res, next) => {
  try {
    const users = await getListFilByName(req.query);
    res.json(users);
  } catch (error) {
    next(error);
  }
};

/**
 * Update user
 */
exports.update = async (req, res, next) => {
  try {
    const { userId } = req.params;
    const { name, phoneNumber } = req.body;
    const user = await User.findOne({ _id: mongoose.Types.ObjectId(userId) });
    if (!user) {
      throw new Error("user not found");
    }

    const updatedUser = await User.findOneAndUpdate(
      { _id: mongoose.Types.ObjectId(userId) },
      {
        $set: { name, phoneNumber },
      },
      { new: true }
    );

    res.json(updatedUser.transform());
  } catch (error) {
    next(error);
  }
};

exports.delete = async (req, res, next) => {
  try {
    const { userId } = req.params;
    const user = await User.findOne({
      _id: mongoose.Types.ObjectId(userId),
    });
    if (!user) {
      throw new Error("user not found");
    }
    await User.findOneAndDelete({
      _id: mongoose.Types.ObjectId(userId),
    });

    res.json(user.transform());
  } catch (error) {
    next(error);
  }
};

exports.createMultiple = async (req, res, next) => {
  try {
    const emails = req.body.map((user) => user.email);
    const existEmails = await User.find({ email: { $in: emails } });
    console.log("existEmails: ", existEmails);
    if (existEmails.length) {
      const message = `Has email already exists: ${existEmails
        .map((user) => user.email)
        .join(" ,")}`;
      throw new Error(message);
    }

    const responseUsers = await User.insertMany(req.body);

    res.json(responseUsers);
  } catch (error) {
    next(error);
  }
};
