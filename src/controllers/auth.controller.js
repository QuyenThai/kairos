const moment = require("moment-timezone");
const mongoose = require("mongoose");
const { jwtExpirationInterval } = require("../config/vars");
const User = require("../models/user.model");
const AccessToken = require("../models/acessToken.model");

function generateTokenResponse(accessToken) {
  const tokenType = "Bearer";
  const expiresIn = moment().add(jwtExpirationInterval, "minutes");
  return {
    tokenType,
    accessToken,
    expiresIn,
  };
}

/**
 * Sign up
 */
exports.signup = async (req, res, next) => {
  try {
    const signupUser = new User(req.body);
    const savedUser = await signupUser.save();
    const token = generateTokenResponse(savedUser.token());
    await AccessToken.generate(savedUser, token);
    return res.json({ token, user: savedUser.transform() });
  } catch (error) {
    return next(User.checkDuplicateEmail(error));
  }
};

/**
 * Returns jwt token and add new accessToken if valid username and password
 */
exports.login = async (req, res, next) => {
  try {
    const { user, accessToken } = await User.findAndGenerateToken(req.body);
    const token = generateTokenResponse(accessToken);
    await AccessToken.generate(user, token);
    return res.json({ token, user: user.transform() });
  } catch (error) {
    return next(error);
  }
};

/**
 * remove accessToken
 */
exports.logout = async (req, res, next) => {
  try {
    await AccessToken.deleteMany({
      userId: mongoose.Types.ObjectId(req.user._id),
    });
    res.json({ message: "logout successful" });
  } catch (error) {
    return next(error);
  }
};
