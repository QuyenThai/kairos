const express = require("express");
const router = express.Router();
const authRoutes = require("./auth");
const userRouters = require("./users");

router.use("/users", userRouters);
router.use("/auth", authRoutes);

module.exports = router;
