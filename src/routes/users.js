const express = require("express");
const router = express.Router();
const controller = require("../controllers/user.controller");
const { validate } = require("express-validation");
const { authorize } = require("../middlewares/auth");
const { USER_ROLE } = require("../utils/defaultValues");
const {
  listUser,
  create,
  update,
  deleteUser,
} = require("../validations/user.validation");

router
  .route("/")
  .get(authorize([USER_ROLE.ADMIN]), validate(listUser), controller.list)
  .post(authorize([USER_ROLE.ADMIN]), validate(create), controller.create);

router
  .route("/:userId")
  .patch(authorize([USER_ROLE.ADMIN]), validate(update), controller.update)
  .delete(
    authorize([USER_ROLE.ADMIN]),
    validate(deleteUser),
    controller.delete
  );

router
  .route("/new-users")
  .post(authorize([USER_ROLE.ADMIN]), controller.createMultiple);

module.exports = router;
