const express = require("express");
const { validate } = require("express-validation");
const controller = require("../controllers/auth.controller");
const { register, login } = require("../validations/auth.validation");
const { authorize } = require("../middlewares/auth");

const router = express.Router();

router.route("/login").post(validate(login), controller.login);
router.route("/signup").post(validate(register), controller.signup);
router.route("/logout").delete(authorize(), controller.logout);
module.exports = router;
