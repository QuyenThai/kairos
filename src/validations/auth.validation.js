const { Joi } = require("express-validation");
const { USER_ROLE } = require("../utils/defaultValues");

module.exports = {
  register: {
    body: Joi.object({
      email: Joi.string().email().required(),
      name: Joi.string().required().max(128),
      phoneNumber: Joi.string().required(),
      password: Joi.string().required().min(6).max(40),
      role: Joi.number().valid(USER_ROLE.ADMIN, USER_ROLE.USER),
    }),
  },

  login: {
    body: Joi.object({
      email: Joi.string().email().required(),
      password: Joi.string().required().max(40),
    }),
  },
};
