const { Joi } = require("express-validation");

module.exports = {
  listUser: {
    query: Joi.object({
      name: Joi.string().max(128),
    }),
  },
  deleteUser: {
    params: Joi.object({
      userId: Joi.string()
        .regex(/^[a-fA-F0-9]{24}$/)
        .required(),
    }),
  },
  create: {
    body: Joi.object({
      email: Joi.string().email().required(),
      name: Joi.string().required().max(128),
      phoneNumber: Joi.string().required().max(10),
      password: Joi.string().required().min(6).max(40),
    }),
  },
  update: {
    params: Joi.object({
      userId: Joi.string()
        .regex(/^[a-fA-F0-9]{24}$/)
        .required(),
    }),
    body: Joi.object({
      name: Joi.string().required().max(128),
      phoneNumber: Joi.string().max(10),
    }),
  },
};
